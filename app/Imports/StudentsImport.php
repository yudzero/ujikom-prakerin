<?php

namespace App\Imports;

use App\Models\Siswa;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class StudentsImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Siswa([
            'nis' => $row['nis'],
            'id_sekolah' => $row['id_sekolah'],
            'id_jurusan' => $row['id_jurusan'],
            'NamaSiswa' => $row['NamaSiswa'],
            'kelas' => $row['kelas'],
            'Tgllahir' => $row['Tgllahir'],
            'Tmplahir' => $row['Tmplahir'],
            'Alamat_Siswa' => $row['Alamat_Siswa'],
            'NoTelp' => $row['NoTelp'],
            'email' => $row['email'],
            'id_user' => $row['id_user']
        ]);
    }
}
