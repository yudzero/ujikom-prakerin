<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Perusahaan extends Model
{
    use HasFactory;

    protected $table = 'perusahaan';
    protected $fillable = ['NoPerusahaan', 'NamaPerusahaan', 'alamat', 'fax', 'email', 'jumlahmurid'];
    protected $primaryKey = 'NoPerusahaan';
    private $increment = false;

    public function getForeignKey()
    {
        return $this->primaryKey;
    }

    public function siswa(){
        return $this->hasManyThrough(Siswa::class, Pemetaan::class, 'NoPerusahaan', 'nis', 'NoPerusahaan', 'nis');
    }

    public function guru(){
        return $this->hasOneThrough(Guru::class, Pemetaan::class, 'NoPerusahaan', 'nip', 'NoPerusahaan', 'nip');
    }

    public function pemetaan(){
        return $this->hasMany(Pemetaan::class);
    }
}
