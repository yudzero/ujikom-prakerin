<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Alfa6661\AutoNumber\AutoNumberTrait;

class Pemetaan extends Model
{
    

    use HasFactory;

    protected $fillable = ['id_pendaftaran', 'id_periode', 'NoPerusahaan', 'nis', 'nip', 'id_pembimbing'];
    protected $table = 'pemetaan';
    protected $primaryKey = 'id_pendaftaran';
    private $increment = false;

    public function getForeignKey()
    {
        return $this->primaryKey;
    }

    public function siswa(){
        return $this->hasManyThrough(Siswa::class, Pemetaan::class, 'NoPerusahaan', 'nis', 'NoPerusahaan', 'nis');
    }

    public function perusahaan(){
        return $this->belongsTo(Perusahaan::class, 'NoPerusahaan', 'NoPerusahaan');
    }

    public function guru(){
        return $this->belongsTo(Guru::class, 'nip', 'nip');
    }

    public function getAutoNumberOptions()
    {
        return [
            'code' => [
                'format' => 'RPL', // Format kode yang akan digunakan.
                'length' => 10 // Jumlah digit yang akan digunakan sebagai nomor urut
            ]
        ];
    }
}
