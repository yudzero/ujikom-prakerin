<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    use HasFactory;

    protected $table = 'guru';
    protected $primaryKey = 'nip';
    private $increment = false;
    protected $fillable = ['nip', 'nama', 'NoTelp'];

    public function getForeignKey(){
        return $this->primaryKey;
    }

    public function pemetaan(){
        return $this->hasMany(Pemetaan::class, 'nip', 'nip');
    }

    // public function perusahaan(){
    //     return $this->hasOneThrough(Perusahaan::class, Pemetaan::class, 'nip', 'NoPerusahaan', 'nip', 'NoPerusahaan');
    // }

    public function perusahaan(){
        return $this->hasOneThrough(Perusahaan::class, Pemetaan::class, 'nip', 'NoPerusahaan', 'nip', 'NoPerusahaan');
    }
}
