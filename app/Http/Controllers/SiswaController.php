<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Siswa;
use App\Models\Jurnal;
use App\Models\Pemetaan;
use App\Models\Perusahaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use function GuzzleHttp\Promise\all;
use Illuminate\Support\Facades\Auth;

class SiswaController extends Controller
{
    public function jurnalsiswa(){
        return view('siswa.jurnalsiswa', [
            'title' =>  'Siswa | Jurnal Siswa',
            'titleheader'   =>  'Jurnal Siswa'
        ]);
    }

    public function sikapsiswa(){
        return view('siswa.sikapsiswa', [
            'title' =>  'Siswa | Sikap Siswa',
            'titleheader'   =>  'Sikap Siswa'
        ]);
    }

    public function daftarindustrisiswa(){
        $perusahaan = Perusahaan::all();
        $siswa = Siswa::all();
        return view('siswa.daftarindustri-siswa', [
            'title' =>  'Siswa | Daftar Industri',
            'titleheader'   =>  'Daftar Industri',
            'perusahaan'    =>  $perusahaan,
            'siswa' =>  $siswa
        ]);
    }

    public function profilsiswa(Siswa $siswa){
        $siswa = Siswa::all()->first();
        return view('siswa.profilsiswa', [
            'title' =>  'Siswa | Profil Siswa',
            'titleheader'   =>  'Profil Siswa',
            'siswa'          =>  $siswa
        ]);
    }

    public function updateprofil(Request $request, Siswa $siswa){
        $attr = $request->validate([
            'NamaSiswa' =>  ['string', 'min:3', 'max:191', 'required'],
            'email' =>  ['email', 'string', 'min:3', 'max:191', 'required'],
            'NoTelp' =>  ['string', 'min:3', 'max:191', 'required'],
        ]);

        $siswa->update($attr);
 
            return back()->with('message', 'Profil Sukses Di Update');
    }

    public function berandasiswa(){
        return view('siswa.berandasiswa', [
            'title' =>  'Siswa | beranda Siswa',
            'titleheader'   =>  'beranda Siswa'
        ]);

        
    }

    public function absenhadir(){
        return view('siswa.absen-hadir', [
            'title' => 'Siswa | Absen',
            'titleheader' => 'Absen'
        ]);
    }

    public function formdaftar(){
        return view('siswa.daftarpkl', [
            'title' => 'Siswa | Daftar PKL',
            'titleheader' => 'Daftar'
        ]);
    }

    public function isijurnal(){
        $jurnal = Jurnal::all();

        return view('siswa.isijurnal', [
            'title' => 'Siswa | Isi Jurnal',
            'titleheader' => 'Pengisian Jurnal Prakerin',
            'jurnal'    => $jurnal
        ]);
    }

    public function absentidakhadir(){
        return view('siswa.absen-tidakhadir', [
            'title' => 'Siswa | Absensi',
            'titleheader' => 'Absensi'
        ]);
    }

    public function siswajurnal(Request $request){
        DB::table('siswas')->insert([
            'aktivitas'  =>  $request->aktivitas,
            'tanggal' => $request->tanggal,
            'divisi' => $request->divisi,
            'waktumulai' => $request->waktumulai,
            'waktuselesai' => $request->waktuselesai,
            'foto' => $request->foto
        ]);

        return redirect('/hubin/perusahaan');
    }

    public function masukpemetaan(Request $request){
        
        DB::table('pemetaan')->insert([
            'id_periode'  =>  '202301',
            'NoPerusahaan' => $request->NoPerusahaan,
            'nis' => $request->nis,
            'nip' => '1234',
            'id_pembimbing' => '1234',
        ]);

        return redirect('/siswa/daftarpkl');
    }
}
