<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Siswa;

class PerusahaanController extends Controller
{
    public function pembimbingperusahaan(){
        $siswa = Siswa::all();
        return view('pembimbing-perusahaan.pembimbing-perusahaan', [
            'title' =>  'Dashboard | Pembimbing Perusahaan',
            'titleheader'   =>  'Pembimbing Perusahaan',
            'siswa' => $siswa
        ]);
    }

    public function detaildata(){
        return view('pembimbing-perusahaan.detail-data', [
            'title' => 'Detail Data',
            'titleheader' => 'Detail Data'
        ]);
    }

    public function daftarsiswa(){
        $siswa = Siswa::all();
        return view('pembimbing-perusahaan.daftarsiswa', [
            'title' => 'Daftar Siswa',
            'titleheader' => 'Daftar Siswa',
            'siswa' => $siswa
        ]);
    }

    public function berinilai(){
        return view('pembimbing-perusahaan.berinilai', [
            'title' => 'Beri Nilai',
            'titleheader' => 'Beri Nilai'
        ]);
    }
}
