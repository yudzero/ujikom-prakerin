<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SekolahController extends Controller
{
    public function dashboardsekolah(){
        return view('pembimbing-sekolah.berandasekolah', [
            'title' => 'Dashboard | Pembimbing Sekolah',
            'titleheader' => 'Dashboard'
        ]);
    }
    public function evaluasipkl(){
        return view('pembimbing-sekolah.evaluasipkl', [
            'title' =>  'Dashboard | Evaluasi PKL',
            'titleheader'   =>  'Evaluasi PKL'
        ]);
    }

    public function viewsikapsiswa(){
        return view('pembimbing-sekolah.viewsikapsiswa', [
            'title' => 'Dashboard | Nilai Sikap Siswa',
            'titleheader' => 'Nilai Sikap Siswa'
        ]);
    }

    public function daftrasiswasekolah(){
        return view('pembimbing-sekolah.daftarsiswasekolah',[
            'title' => 'Dashboard | Daftar Siswa',
            'titleheader' => 'Daftar Siswa'
        ]);
    }
}
