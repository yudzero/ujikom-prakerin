<?php

namespace App\Http\Controllers;

use App\Models\Guru;
use App\Models\User;
use App\Models\Siswa;
use App\Models\Pemetaan;
use App\Models\Perusahaan;
use App\Imports\UserImport;
use App\Imports\StudentsImport;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\PDF;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;

class HubinController extends Controller
{

    public function pemetaan(Request $request){
        $pemetaan = Pemetaan::with('siswas')->latest()->get();
        
        return view('hubin.peta', [
            'title' =>  'Pemetaan PKL',
            'titleheader'   =>  'Pemetaan',
            'pemetaan'  =>  $pemetaan
        ]);
    }

    public function tambahpeta(Request $request){
        $data = DB::table('pemetaan')->insert([
            'id_pendaftaran'    => $request->id_pendaftaran,
            'id_periode'    => $request->id_periode,
            'NoPerusahaan'    => $request->NoPerusahaan,
            'nis'    => $request->nis,
            'nip'    => $request->nip,
            'id_pembimbing'    => $request->id_pembimbing,
        ]);

        return redirect('/hubin/pemeta');
    }

    public function dashboardhubin(){
        $perusahaan = Perusahaan::all();
        $siswa  = Siswa::with('perusahaan')->get();
        return view('hubin.dashboardhubin', [
            'title' =>  'Dashboard | Hubin',
            'titleheader'   =>  'Dashboard',
            'perusahaan'          =>  $perusahaan,
            'siswa'          =>  $siswa,
        ]);
    }
    public function index(){
        return view('perusahaanhubin', [
            'title' => 'Daftar Perusahaan'
        ]);
    }

    public function hubinperusahaan(){
        $perusahaan = Perusahaan::all();
        return view('hubin.perusahaanhubin', [
            'title' =>  'Daftar Perusahaan',
            'titleheader'   =>  'Daftar Perusahaan',
            'perusahaan'    =>  $perusahaan
        ]);
    }

    public function hubinpemetaan(){
        return view('hubin.pemetaanpkl', [
            'title' =>  'Daftar Perusahaan',
            'titleheader'   =>  'Daftar Perusahaan'
        ]);
    }

    public function hubineditakunsiswa (){
        return view('hubin.editakunsiswa', [
            'title' =>  'Edit Siswa',
            'titleheader'   =>  'Edit Siswa'
        ]);
    }

    public function siswaterdaftarhubin(){
        // $siswa = Pemetaan::where('status', 'diterima')->with('perusahaan', 'jurusan')->get();
        $data = Pemetaan::where('status', 'pending')->with(['siswa', 'guru'])->latest()->get();
        return view('hubin.siswaterdaftarhubin', [
            'title' =>  'Hubin | Siswa Terdaftar',
            'titleheader'   =>  'Daftar Perusahaan',
            'data' => $data
        ]);
    }
    
    public function daftarsiswahubin(){
        $perusahaan = Perusahaan::all();
        $siswa = Siswa::with('perusahaan', 'jurusan')->get();
        
        return view('hubin.daftarsiswahubin', [
            'title' =>  'Hubin | Daftar Siswa',
            'titleheader'   =>  'Daftar Siswa',
            "siswa" => $siswa,
            "perusahaan"    => $perusahaan,
            
        ]);
    }

    public function cetaksurat(){
        $perusahaan = Perusahaan::all();
        $data = User::all();
        return view('hubin.cetaksurat', [
            'title' =>  'Hubin | Cetak Surat',
            'titleheader'   =>  'Cetak Surat',
            'perusahaan'    =>  $perusahaan,
            'data'          =>  $data
        ]);
    }

    public function pemetaansiswa(){

        // $data = Perusahaan::has('siswa')->has('guru')->with('siswa', 'guru')->latest()->get();
        $data = Pemetaan::where('status', 'pending')->with(['siswa', 'guru'])->latest()->get();
        $guru  = Guru::all();
        return view('hubin.pemetaansiswa', [
            'title' =>  'Hubin | Pemetaan Siswa',
            'titleheader'   =>  'Pemetaan Siswa',
            'data'  => $data,
            'guru'  => $guru
        ]);
    }

    public function terimaSiswa(Request $request) {
        Pemetaan::where('nis', $request->nis)->where('status', 'pending')->update([
            'status' => 'diterima',
            'nip'   =>  $request->nip
        ]);
        
        return back()->with('success', 'Siswa diterima');
    }

    public function tolakSiswa(Request $request) {
        Pemetaan::where('nis', $request->nis)->where('status', 'pending')->update([
            'status' => 'ditolak'
        ]);
        
        return back()->with('success', 'Siswa Ditolak!');
    }

    public function pemetaanguru(){
        
        
        return view('hubin.pemetaanguru', [
            'title' =>  'Hubin | Pemetaan Guru',
            'titleheader'   =>  'Pemetaan Guru',
      
        ]);
    }

    public function importdata(Request $request){
        $file = $request->file('file');

		Excel::import(new UserImport, $file);

        return back()->with('success','Data Berhasil Di Import') ;
    }
    public function tampilimport(){
           $user = User::all();
        return view('hubin.upload',[
            'user'  => $user,
            'title' =>  'Import Data User',
            'titleheader'   =>  'Import Data User'
        ]);
    } 

    public function cetakmurid(){
        $data = User::all();

        $pdf = PDF::loadView('hubin.surat', [
            'data'  => $data
        ]);

        return $pdf->download('suratpengajuan.pdf');
    }

    public function suratpdf(){
        $data = User::all();

        return view('hubin.surat', [
            'data'  =>  $data
        ]);
    }

    public function edit_perusahaan(Request $request, $NoPerusahaan){
        $perusahaan = Perusahaan::where('NoPerusahaan', $request->id);
        $perusahaan->update($request->except(['_token']));

        return redirect('hubin/perusahaan') ->with('success','Data Berhasil Diubah') ;

    }

    public function tambahdata(Request $request){
        // $validatedData = $request->validate([
        //     'NoPerusahaan' => 'required',
        //     'NamaPerusahaan' => 'required',
        //     'alamat' => 'required',
        //     'email' => 'required',
        //     'jumlahmurid' => 'required',
        //     'fax' => 'required',
        //     'maps' => 'required',
        // ]);
        
        // Perusahaan::create($validatedData);

        DB::table('perusahaan')->insert([
            'NoPerusahaan' => $request->NoPerusahaan,
            'NamaPerusahaan' => $request->NamaPerusahaan,
            'alamat' => $request->alamat,
            'email' => $request->email,
            'jumlahmurid' => $request->jumlahmurid,
            'fax' => $request->fax,
            'maps' => $request->maps,
        ]);

        return redirect('/hubin/perusahaan') ->with('success','Data Berhasil Ditambah');
    }

    public function tambahdatasiswa(Request $request){
        DB::table('siswas')->insert([
            'nis'   =>  $request->nis,
            'NamaSiswa'  =>  $request->NamaSiswa,
            'kelas' => $request->kelas,
            'Tgllahir' => $request->Tgllahir,
            'Tmplahir' => $request->Tmplahir,
            'Alamat_Siswa' => $request->Alamat_Siswa,
            'NoTelp' => $request->NoTelp,
            'email'  =>  $request->email
        ]);

        return redirect('/hubin/siswa')->with('success','Berhasil Menambah Data!');
    }

    public function hapussiswa($id){
        DB::table('siswas')->where('nis', $id)->delete();

        return redirect('/hubin/siswa');
    }

    public function hapusperusahaan($id){
        DB::table('perusahaan')->where('NoPerusahaan', $id)->delete();

        return redirect('/hubin/perusahaan');
    }

    public function updatesiswa(Request $request, $nis){
        $siswa = Siswa::with('pemetaan')->where('nis', $request->id);
        $siswa->update($request->except(['_token']));

        return redirect('/hubin/siswa');
    }

    public function tampilimportsiswa(){
        return view('hubin.importsiswa', [
            'title' =>  'Hubin | Import Siswa',
            'titleheader'   =>  'Import Data Siswa'
        ]);
    }

    public function importsiswa(Request $request){

        $file = $request->file('file');

		Excel::import(new StudentsImport, $file);

        return back()->withStatus('Excel File Berhasil Diimport!');
    }
    public function pemetaankompetensi(){

        return view('hubin.pemetaankompetensi', [
            'title' =>  'Hubin | Pemetaan Kompetensi',
            'titleheader'   =>  'Pemetaan Kompetensi',
      
        ]);
    }
    
}