<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemetaan', function (Blueprint $table) {
            $table->char('id')->autoIncrement();
            $table->unsignedBigInteger('id_pendaftaran')->primary();
            $table->char('id_periode');
            $table->unsignedBigInteger('NoPerusahaan');
            $table->unsignedBigInteger('nis');
            $table->char('nip');
            $table->char('id_pembimbing');
            $table->enum('status', ['pending', 'ditolak', 'diterima']);
            $table->timestamps();
            
            $table->foreign('nis')->references('nis')->on('siswas');
            $table->foreign('NoPerusahaan')->references('NoPerusahaan')->on('perusahaan')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemetaan');
    }
};
