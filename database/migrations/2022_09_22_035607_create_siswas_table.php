<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswas', function (Blueprint $table) {
            $table->unsignedBigInteger('nis')->primary();
            $table->char('id_sekolah')->nullable();
            $table->unsignedBigInteger('id_jurusan')->nullable();
            $table->string('NamaSiswa');
            $table->string('kelas');
            $table->date('Tgllahir')->nullable();
            $table->string('Tmplahir');
            $table->string('Alamat_Siswa');
            $table->integer('NoTelp')->nullable();
            $table->string('email');
            $table->unsignedBigInteger('id_user');
            $table->timestamps();

            $table->foreign('id_user')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswas');
    }
};
