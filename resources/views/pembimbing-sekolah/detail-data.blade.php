@extends('layouts.pembimbingperusahaan.main')
@section('content')
<div class="container">
        <h4 class="mt-5">Detail Data Siswa</h4>
        <div class="card mt-4" style="background-color: #EDEDED; border-radius: 20px; border: #EDEDED;">
            <div class="row mt-5 mb-5" style="margin: 0 auto; width: 100%;">
                <div class="col-1">
                </div>
                <div class="col" >
                    <p>Nama</p>
                    <p>Divisi</p>
                    <p>Kelas</p>
                    <p>Tempat Tanggal Lahir</p>
                    <p>Alamat</p>
                </div>
                <div class="col-1">
                    <p>:</p>
                    <p>:</p>
                    <p>:</p>
                    <p>:</p>
                    <p>:</p>
                </div>
                <div class="col">
                    <p>Galang Yudha Ilham</p>
                    <p>IT</p>
                    <p>XII-RPL2</p>
                    <p>Bandung, 2 januari 2003</p>
                    <p>Jln Budi Cilember</p>
                </div>
            </div>
        </div>
        <button class="btn button1 mt-5" style="background-color:#EDEDED;border-radius: 9px;" >Nyatakan Telah Selesai PKL</button>
    </div>
</body>
@endsection