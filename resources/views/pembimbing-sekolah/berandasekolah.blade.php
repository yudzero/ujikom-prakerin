@extends('layouts.pembimbingsekolah.main')
@section('content')
<style>
</style>
<div class="container">
    <div class="row">
        <div class="col-4">
            <h3 class="mt-3" style="color: black; font-weight:600;">Hello, Mr Ahmad Syahroni!</h3>
            <div class="dropdown">
              <button style="background-color: #0066FF;" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                Pilih Perusahaan
              </button>
              <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                <li><a class="dropdown-item" href="#">Scola LMS</a></li>
                <li><a class="dropdown-item" href="#">Chlorine Digital Media</a></li>
                <li><a class="dropdown-item" href="#">FS Indonesia</a></li>
              </ul>
            </div>
        </div>
        <div class="col-4">
            <div class="card mt-3">
                <a href="/sekolah/siswa" style="text-decoration: none; color: black;">
                    <h5 class="card-title mt-4" style="text-align: center;">Jumlah Siswa</h5>
                    <h1 class="mb-4" style="text-align: center;">45</h1>
                    </a>
            </div>
        </div>
        <div class="col-4">
            <div class="card mt-3">
                <a href="" style="text-decoration: none; color: black;">
                    <h5 class="card-title mt-4" style="text-align: center;">Perlu Diperhatikan</h5>
                    <h1 class="mb-4" style="text-align: center;">2</h1>
                    </a>
            </div>
        </div>
        </div>

        <div class="row">
            <div class="col-3 mt-5">
                <div class="card" style="color: black;">
                    <h5 class="card-title mt-4" style="text-align: center;">Lihat Evaluasi Prakerin</h5>
                    <p style="text-align: center;">PT.Maju Mundur</p>
                    <a style="text-align: center;" class=" mt-5 mb-4" href=""><i style="font-size: 60px;" class="fa-solid fa-circle-arrow-right mb-5"></i></a>
                </div>
            </div>
            <div class="col-9">
                <div class="tw-bg-white tw-shadow-md tw-h-fit tw-py-10 tw-w-full mt-5" style="width: 500px; margin:0 auto;">
                   <div class="tw-px-10 tw-font-pop mt-5">
                     <div>
                       <canvas id="myChart" class="tw-mt-5 mb-5"></canvas>          
                     </div>          
                   </div>
                 </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script>
  // chart view dasbrot
const labels = [
  'Senin',
  'Selasa',
  'Rabu',
  'Kamis',
  'Jumat',
];
const data = {
  labels: labels,
  datasets: [
    {
    label: 'Siswa Masuk',
    backgroundColor: '#0066FF',
    borderColor: '#0066FF',
    data: [20, 15, 19, 19, 11],
  },
  {
    label: "Siswa Tidak Masuk",
    backgroundColor: '#D9D9D9',
    borderColor: '#D9D9D9',
    data: [0, 5, 1, 1, 9],
  }
]
};

const config = {
  type: 'bar',
  data: data,
  options: {}
};

const myChart = new Chart(
    document.getElementById('myChart'),
    config
  );

  
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
@endsection