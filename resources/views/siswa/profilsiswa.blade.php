<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{ $title }}</title>
    

    <!-- Custom fonts for this template-->
    <link href="{{ asset('assets/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300;500;600&display=swap" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link href="{{ asset('assets/css/sb-admin-2.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />

</head>
<style>
  *{
    color: black;
  }
</style>
    <div class="container mt-3">
      <h3 class="mb-5" style="color: black; font-weight:600;">Profile</h3>
        <div class="row">
            <div class="col-9" style="margin: 0 auto;">
        <form action="/updateprofil/{{ $siswa->nis }}" method="POST">
          @method("put")
          @csrf

          @if (session()->has('message'))
            <div class="text-success-600 mb-4">{{ session('message') }}</div>
          @endif
            <div class="form-group">
              <label for="exampleInputNama">Nama:</label>
              <input type="text" class="form-control" name="NamaSiswa" value="{{ $siswa->NamaSiswa }}" id="exampleInputEmail1">
              @error('NamaSiswa')
               <div class="text-danger-500 mt-2 text-sm">{{ $message }}</div>   
              @enderror
            </div>
            <div class="form-group">
                <label for="exampleInputTTL">Email:</label>
                <input type="email" name="email" class="form-control" value="{{ $siswa->email }}" id="exampleInputPassword1">
              </div>
              <div class="form-group">
                <label for="exampleInputKomke">Nomor Telepon:</label>
                <input type="number" name="NoTelp" class="form-control" value="{{ $siswa->NoTelp }}" id="exampleInputPassword1">
              </div>
              <div class="form-group">
                <label for="exampleInputNIS">Tempat Lahir:</label>
                <input type="text" class="form-control" value="{{ $siswa->Tmplahir }}" id="exampleInputPassword1" disabled>
              </div>
              <div class="form-group">
                <label for="exampleInputNIS">Tanggal Lahir:</label>
                <input type="text" class="form-control" value="{{ $siswa->Tgllahir }}" id="exampleInputPassword1" disabled>
              </div>
          <div class="col-button mb-3 ">
            <button type="submit" class="btn btn-outline-dark">Simpan</button>
            <button type="button" class="btn btn-outline-dark">Kembali</button>
        </div>
            </div>
        </div>
             
    </div>
         </form>