<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>{{ $title }}</title>
    <!-- Custom fonts for this template-->
    <link href="{{ asset('assets/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300;500;600&display=swap" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="{{ asset('assets/css/sb-admin-2.min.css') }}" rel="stylesheet">
    <script type="text/javascript" src="https://unpkg.com/webcam-easy/dist/webcam-easy.min.js"></script>

</head>
<style>
    a{
        background-color: rgb(184, 184, 184);
    }
    video{
        margin-top: 30px;
        border-radius: 16px;
        margin-left: 20px;
    }
</style>
<body>
    <div class="container">
        <div class="row">
            <div class="col-2"></div>
            <div class="col-6">
                <video id="webcam" autoplay playsinline width="640" height="480"></video>
                <canvas id="canvas" class="d-none"></canvas>
                <a onclick="takeAPicture()"><i class="fa-solid fa-camera"></i></a>
                <button class="btn btn-primary mt-2 mb-5  ml-5 mr-5" style="float: right; background-color:#6a9098; border:transparent; width:130px;">Submit</button>
                <button type="submit" class="btn btn-primary mt-2 mb-5" style="float: right; background-color:#6a9098; border:transparent; width:130px;">Back</button>
            </div>
        <div class="col-3"></div>
        </div>
    </div>
</body>


<script>
    const webcamElement = document.getElementById('webcam');
    const canvasElement = document.getElementById('canvas');
    const snapSoundElement = document.getElementById('snapSound');
    const webcam = new Webcam(webcamElement, 'user', canvasElement, snapSoundElement);

    webcam.start()
  .then(result =>{
    console.log("webcam started");
  })
  .catch(err => {
    console.log(err);
});

let picture = webcam.snap();
document.querySelector('#download-photo').href = picture;

$('#cameraFlip').click(function() {
  webcam.flip();
  webcam.start();
});

navigator.mediaDevices.enumerateDevices()
  .then(getVideoInputs)
  .catch(errorCallback);
function getVideoInputs(mediaDevices){
  mediaDevices.forEach(mediaDevice => {
    if (mediaDevice.kind === 'videoinput') {
      this._webcamList.push(mediaDevice);
    }
  });
}

navigator.mediaDevices.getUserMedia(this.getMediaConstraints())
  .then(stream => {
    this._webcamElement.srcObject = stream;
    this._webcamElement.play();
  })
  .catch(error => {
    //...
  });
</script>