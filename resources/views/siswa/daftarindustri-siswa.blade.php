@extends('layouts.siswa.main')
@section('content')
    <div class="container">
        <div class="row">
            <table id="myTable" class="table">
              <thead>
                <tr style="background-color: #DADADC; border-radius:30px;">
                  <th scope="col">Nama Perusahaan</th>
                  <th scope="col">Jurusan yang dibutuhkan</th>
                  <th scope="col">Map</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($perusahaan as $p)
                <tr>
                  <td>{{ $p->NamaPerusahaan }}</td>
                  <td>RPL</td>
                    <td> <a href="{{ $p->maps }}">
                        <button class="buttonmaps">maps <i class="fa-solid fa-arrow-right ml-1"></i></button></a> 
                    </td>
                    <td><a class="dropdown-item" href="#" data-toggle="modal" data-target="#daftarmodal-{{ $p->NoPerusahaan }}">
                            <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                            Daftar
                        </a>    
                    </td>
                </tr>
                @endforeach
              </tbody>
            </table>
            @foreach($perusahaan as $p)
            <div class="modal fade" id="daftarmodal-{{ $p->NoPerusahaan }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <form action="/siswadaftar" method="POST">
                        @csrf
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Form Pendaftaran PKL</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                               
                                <label for="disabledTextInput" class="form-label">Nis</label>
                                <input type="text" value="{{ Auth::user()->siswa->nis }}" name="nis" id="inputPassword5" class="form-control"
                                    aria-describedby="passwordHelpBlock" disabled>

                                <input type="text" value="{{ $p->NoPerusahaan }}" name="NoPerusahaan" id="inputPassword5" class="form-control"
                                    aria-describedby="passwordHelpBlock" style="display: none;">
                                
                                {{-- <label for="inputPassword5" class="form-label">Periode</label>
                                <input type="text" value="{{ Auth::user()->siswa->nis }}" name="id_periode" id="inputPassword5" class="form-control"
                                    aria-describedby="passwordHelpBlock"> --}}
                               
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                                <button class="btn btn-primary" type="submit">Daftar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @endforeach
        </div>
    </div>
@endsection