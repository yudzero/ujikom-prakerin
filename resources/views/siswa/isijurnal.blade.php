@extends('layouts.siswa.main')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Isi Jurnal</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300;500;600&display=swap" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link href="{{ asset('assets/css/sb-admin-2.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>
<style>
    label{
        font-weight: 800;
        color: black;
    }
    </style>
<body>
    <div class="container">

        <h3 class="mt-5" style="text-align: center; font-weight:800; color:black;">Pengisian Jurnal Prakerin</h3>
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8 mt-5">
        <form action="/isijurnal" method="POST">
          @csrf

            <div class="mb-3">
              <label for="exampleFormControlInput1" class="form-label">Aktivitas PKL</label>
              <input type="email" name="aktivitas" class="form-control" id="exampleFormControlInput1">
            </div>
            <div class="mb-3">
              <label for="exampleFormControlInput1" class="form-label">Hari/Tanggal Pelaksanaan</label>
              <input type="date" class="form-control"  id="exampleFormControlInput1">
            </div>
            <div class="mb-3">
              <label for="exampleFormControlInput1" class="form-label">Divisi/Dept</label>
              <input type="text" name="divisi" class="form-control"  id="exampleFormControlInput1">
            </div>
            <div class="mb-3">
              <label for="exampleFormControlInput1" class="form-label">Mulai Pukul</label>
              <input type="time" name="waktu" class="form-control"  id="exampleFormControlInput1">
            </div>
            <div class="mb-3">
              <label for="exampleFormControlInput1" class="form-label">Selesai Pukul</label>
              <input type="time" class="form-control" id="exampleFormControlInput1">
            </div>
            <div class="mb-3 mt-4">
                <input type="file" placeholder="Upload Gambar">
            </div>

            <button class="btn btn-primary mt-5 mb-5  ml-5" style="float: right; background-color:#395B64; border:transparent; width:130px;">Submit</button>
            <button type="submit" class="btn btn-primary mt-5 mb-5" style="float: right; background-color:#395B64; border:transparent; width:130px;">Cancel</button>
        </form>
            </div>
            <div class="col-2"></div>
    </div>
</body>
</html>
@endsection