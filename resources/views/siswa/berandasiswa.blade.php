<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Beranda Siswa</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500;600&display=swap" rel="stylesheet">
</head>
<style>
    *{
        font-family: 'Montserrat', sans-serif;
    }
    .tablinks{
        border: none;
        color: black;
        background-color: transparent;
    }
    .p-siswa{
        color: #6c6d6e;
        font-size: 13px;
        margin-left: 39%;
        margin-top: -10px;
    }
    .btn-edit{
        font-size: 12px;
        margin-left: 37%;
    }
    :root {
        --dark-body: #4d4c5a;
        --dark-main: #141529;
        --dark-second: #79788c;
        --dark-hover: #323048;
        --dark-text: #f8fbff;

        --light-body: #f3f8fe;
        --light-main: #fdfdfd;
        --light-second: #c3c2c8;
        --light-hover: #edf0f5;
        --light-text: #151426;  

        --blue: #0000ff;
        --white: #fff;

        --shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;

        --font-family: cursive;
    }

    .light {
        --bg-body: var(--light-body);
        --bg-main: var(--light-main);
        --bg-second: var(--light-second);
        --color-hover: var(--light-hover);
        --color-txt: var(--light-text);
    }

    *{
        padding: 0;
        margin: 0;
        box-sizing: border-box;
}

html,
body {
    height: 100vh;
    display: grid;
    place-items: center;
    font-family: var(--font-family);
    background-color: var(--bg-body);
}

:root {
    --dark-body: #4d4c5a;
    --dark-main: #141529;
    --dark-second: #79788c;
    --dark-hover: #323048;
    --dark-text: #f8fbff;

    --light-body: #f3f8fe;
    --light-main: #fdfdfd;
    --light-second: #c3c2c8;
    --light-hover: #edf0f5;
    --light-text: #151426;

    --blue: #0000ff;
    --white: #fff;

    --shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;

    --font-family: cursive;
}

.dark {
    --bg-body: var(--dark-body);
    --bg-main: var(--dark-main);
    --bg-second: var(--dark-second);
    --color-hover: var(--dark-hover);
    --color-txt: var(--dark-text);
}

.light {
    --bg-body: var(--light-body);
    --bg-main: var(--light-main);
    --bg-second: var(--light-second);
    --color-hover: var(--light-hover);
    --color-txt: var(--light-text);
}

* {
    padding: 0;
    margin: 0;
    box-sizing: border-box;
}

html,
body {
    height: 100vh;
    display: grid;
    place-items: center;
    font-family: var(--font-family);
    background-color: var(--bg-body);
}

.calendar {
    height: max-content;
    width: 240px;
    background-color: var(--bg-main);
    border-radius: 30px;
    padding: 5px;
    position: relative;
    overflow: hidden;
    margin: 0 auto;
    margin-bottom: 30px;
    /* transform: scale(1.25); */
}

.light .calendar {
    box-shadow: var(--shadow);
}

.calendar-header {
    display: flex;
    justify-content: space-between;
    align-items: center;
    font-size: 14px;
    font-weight: 600;
    color: var(--color-txt);
    padding: 10px;
}

.calendar-body {
    padding: 10px;
}

.calendar-week-day {
    height: 50px;
    display: grid;
    grid-template-columns: repeat(7, 1fr);
    font-weight: 600;
    margin-top: -25px;
}

.calendar-week-day div {
    display: grid;
    place-items: center;
    color: var(--bg-second);
    font-size: 10px;
}

.calendar-days {
    display: grid;
    grid-template-columns: repeat(7, 1fr);
    gap: 2px;
    color: var(--color-txt);
    font-size: 12px;
}

.calendar-days div {
    width: 23px;
    height: 23px;
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 5px;
    position: relative;
    cursor: pointer;
    animation: to-top 1s forwards;
    /* border-radius: 50%; */
}

.calendar-days div span {
    position: absolute;
}

.calendar-days div:hover span {
    transition: width 0.2s ease-in-out, height 0.2s ease-in-out;
}

.calendar-days div span:nth-child(1),
.calendar-days div span:nth-child(3) {
    width: 2px;
    height: 0;
    background-color: var(--color-txt);
}

.calendar-days div:hover span:nth-child(1),
.calendar-days div:hover span:nth-child(3) {
    height: 100%;
}

.calendar-days div span:nth-child(1) {
    bottom: 0;
    left: 0;
}

.calendar-days div span:nth-child(3) {
    top: 0;
    right: 0;
}

.calendar-days div span:nth-child(2),
.calendar-days div span:nth-child(4) {
    width: 0;
    height: 2px;
    background-color: var(--color-txt);
}

.calendar-days div:hover span:nth-child(2),
.calendar-days div:hover span:nth-child(4) {
    width: 100%;
}

.calendar-days div span:nth-child(2) {
    top: 0;
    left: 0;
}

.calendar-days div span:nth-child(4) {
    bottom: 0;
    right: 0;
}

.calendar-days div:hover span:nth-child(2) {
    transition-delay: 0.2s;
}

.calendar-days div:hover span:nth-child(3) {
    transition-delay: 0.4s;
}

.calendar-days div:hover span:nth-child(4) {
    transition-delay: 0.6s;
}

.calendar-days div.curr-date,
.calendar-days div.curr-date:hover {
    background-color: var(--blue);
    color: var(--white);
    border-radius: 50%;
}

.calendar-days div.curr-date span {
    display: none;
}

.month-picker {
    padding: 5px 10px;
    border-radius: 10px;
    cursor: pointer;
}

.month-picker:hover {
    background-color: var(--color-hover);
}

.year-picker {
    display: flex;
    align-items: center;
}

.year-change {
    height: 40px;
    width: 12px;
    border-radius: 50%;
    display: grid;
    place-items: center;
    margin: 0 10px;
    cursor: pointer;
}

pre{
    margin-bottom: 0px;
}

.year-change:hover {
    background-color: var(--color-hover);
}

.calendar-footer {
    padding: 10px;
    display: flex;
    justify-content: flex-end;
    align-items: center;
}

.toggle {
    display: flex;
}

.toggle span {
    margin-right: 10px;
    color: var(--color-txt);
}


.month-list {
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    background-color: var(--bg-main);
    padding: 20px;
    grid-template-columns: repeat(3, auto);
    gap: 5px;
    display: grid;
    transform: scale(1.5);
    visibility: hidden;
    pointer-events: none;
}

.month-list.show {
    transform: scale(1);
    visibility: visible;
    pointer-events: visible;
    transition: all 0.2s ease-in-out;
}

.month-list > div {
    display: grid;
    place-items: center;
}

.month-list > div > div {
    width: 100%;
    padding: 0px 0px;
    border-radius: 10px;
    text-align: center;
    cursor: pointer;
    color: var(--color-txt);
    font-size: 10px;
}

.month-list > div > div:hover {
    background-color: var(--color-hover);
}

@keyframes to-top {
    0% {
        transform: translateY(100%);
        opacity: 0;
    }
    100% {
        transform: translateY(0);
        opacity: 1;
    }
}
.featured{
    background-color: transparent;
    border-color: black;
    width: 225px;
    height: 90px;
    border: 2px solid #000000;
    border-radius: 10px;
    margin-top: 30px;
    font-size: 15px;
    margin-right: 14px;
}
.featured:hover{
    border-color: #bbbbbb;
    color: #bbbbbb;
}
.tab {
  overflow: hidden;
  border: none;
  background-color: transparent;
}
.tab button {
  background-color: inherit;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}
.tab button:hover {
  color: #ddd;
}
.tab button.active {
  color: #ddd;
}
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: none;
  border-top: none;
}
.tabcontent {
  animation: fadeEffect 1s; /* Fading effect takes 1 second */
}

/* Go from zero to full opacity */
@keyframes fadeEffect {
  from {opacity: 0;}
  to {opacity: 1;}
}
</style>
<body>
    <nav >
        <div class="container">
        <div class="row">
            <div class="col-8">
                <img src="{{ asset('assets/img/Logo E-Prakerin Black.png') }}"  style="width: 30%; margin-top: -40px; margin-left: -30px;">
            </div>
            <div class="col-4 mt-5">
            <div class="tab" style="text-align: center;">
                {{-- tabs tabs --}}
              <button class="tablinks" onclick="openCity(event, 'Profile')" id="defaultOpen">Profile</button><span style="margin-right: 40px;"></span>
              <button class="tablinks" onclick="openCity(event, 'Notification')">Notification</button>
            </div>
            </div>
        </div>
        </div>
    </nav>
   <div class="container">
    <div class="row">
        <div class="col-8 mb-5">
            <h2 style="font-weight: 600;">Hallo, {{ Auth::user()->siswa->NamaSiswa }}</h2>
            <p style="color: #464c4d; width: 500px; font-size: 15px;">Selamat datang di E-Prakerin, website yang dibuat khusus untuk memudahkan kalian dalam administrasi praktek kerja industri atau biasa disebut "magang di dunia industri".</p><br>
            <button type="button" class="btn btn-dark" data-bs-toggle="modal" data-bs-target="#exampleModal">Absen Sekarang<i class="fa-solid fa-arrow-up-right-from-square" style="margin-left:8px;"></i></button>
            {{-- pop up absensi --}}
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
             <div class="modal-dialog modal-dialog-centered">
               <div class="modal-content">
                 <div class="modal-body">
                    <button type="button" class="btn-close mb-4" data-bs-dismiss="modal" aria-label="Close" style="float:right;"></button>
                    <h3 class="mt-5" style="text-align: center; font-weight:600;">Pilih Salah Satu</h3>
                    <div class="mt-5 mb-5" style="text-align: center;">
                    <a href="/siswa/absenhadir"><button type="button" class="btn btn-secondary mr-4" style="background-color: #75A0A9; border:transparent;">Hadir</button></a>
                    <a href="/siswa/absentidakhadir"><button type="button" class="btn btn-primary" style="background-color: #75A0A9; border:transparent;" >Tidak Hadir</button></a>
                    </div>
                 </div>
               </div>
             </div>
            </div>
            <br><br><br>
            <h5 style="font-weight: 600;">Featured: </h5>
            <div class="row">
                <div class="col">
            <a href="/siswa/daftarpkl"><button class="featured"><i  class="fa-solid fa-building"></i><br>Daftar PKL</button></a>
            <a href="/siswa/sikap"><button class="featured"><i class="fa-solid fa-star"></i><br>Nilai Sikap Siswa</button></a>
        </div>
        </div>
            <a href="/siswa/jurnal"><button class="featured"><i class="fa-solid fa-chart-simple"></i><br>Laporan Kegiatan Siswa</button></a>
            <a href="/siswa/isijurnal"><button class="featured"><i class="fa-solid fa-book"></i><br>Jurnal Siswa</button></a>
        </div>
        {{-- tab profil siswa --}}
        <div class="col-4">
            <div id="Profile" class="tabcontent" style="margin-top: 0px;">
              <img style="border-radius: 50%; height: 80px; margin-left: 38%;" src="https://www.w3schools.com/howto/img_avatar.png">
              <p class="mt-3 text-center" style="font-weight: 600; font-size: 16px; text-align:center;">{{ Auth::user()->siswa->NamaSiswa }}</p>
              <p class="p-siswa">{{ Auth::user()->level }} - RPL</p>
              <a href="/siswa/profil" class="btn btn-light btn-edit">Edit Akun</a>
            <hr>

            <body class="light">

                <div class="calendar mt-5">
                    <div class="calendar-header">
                        <span class="month-picker" id="month-picker">February</span>
                        <div class="year-picker">
                            <span class="year-change" id="prev-year">
                                <pre> < </pre>
                            </span>
                            <span id="year">2021</span>
                            <span class="year-change" id="next-year">
                                <pre> > </pre>
                            </span>
                        </div>
                    </div>
                    <div class="calendar-body">
                        <div class="calendar-week-day">
                            <div>Sun</div>
                            <div>Mon</div>
                            <div>Tue</div>
                            <div>Wed</div>
                            <div>Thu</div>
                            <div>Fri</div>
                            <div>Sat</div>
                        </div>
                        <div class="calendar-days"></div>
                    </div>
                    <div class="calendar-footer">
                        <div class="toggle">
                    </div>
                    <div class="month-list"></div>
                </div>
            </div>

                <button class="btn btn-danger mt-4 mb-3" style="float: right;"><a style="text-decoration: none;color:white;" href="{{ route('logout') }}">Logout</a></button>
            </body>
                </div>
                {{-- tab notifikasi --}}
                <div id="Notification" class="tabcontent">
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <i class="fa-solid fa-circle-exclamation"></i>
                  <strong>Warning!</strong> Kekuatan password anda rendah.
                  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    <i class="fa-solid fa-circle-exclamation"></i>
                  <strong>Hey kamu belum absen loh!</strong> Ayo segera absen, sebelum pergantian hari!
                  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <i class="fa-solid fa-circle-check"></i>
                  <strong>Congratulations!</strong> Selamat akun anda sudah selesai di buat.
                  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
                </div>
  </body>         
</body>
<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
document.getElementById("defaultOpen").click();
</script>
<script>
    let calendar = document.querySelector('.calendar')

const month_names = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

isLeapYear = (year) => {
    return (year % 4 === 0 && year % 100 !== 0 && year % 400 !== 0) || (year % 100 === 0 && year % 400 ===0)
}

getFebDays = (year) => {
    return isLeapYear(year) ? 29 : 28
}

generateCalendar = (month, year) => {

    let calendar_days = calendar.querySelector('.calendar-days')
    let calendar_header_year = calendar.querySelector('#year')

    let days_of_month = [31, getFebDays(year), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    calendar_days.innerHTML = ''

    let currDate = new Date()
    if (!month) month = currDate.getMonth()
    if (!year) year = currDate.getFullYear()

    let curr_month = `${month_names[month]}`
    month_picker.innerHTML = curr_month
    calendar_header_year.innerHTML = year

    // get first day of month
    
    let first_day = new Date(year, month, 1)

    for (let i = 0; i <= days_of_month[month] + first_day.getDay() - 1; i++) {
        let day = document.createElement('div')
        if (i >= first_day.getDay()) {
            day.classList.add('calendar-day-hover')
            day.innerHTML = i - first_day.getDay() + 1
            day.innerHTML += `<span></span>
                            <span></span>
                            <span></span>
                            <span></span>`
            if (i - first_day.getDay() + 1 === currDate.getDate() && year === currDate.getFullYear() && month === currDate.getMonth()) {
                day.classList.add('curr-date')
            }
        }
        calendar_days.appendChild(day)
    }
}

let month_list = calendar.querySelector('.month-list')

month_names.forEach((e, index) => {
    let month = document.createElement('div')
    month.innerHTML = `<div data-month="${index}">${e}</div>`
    month.querySelector('div').onclick = () => {
        month_list.classList.remove('show')
        curr_month.value = index
        generateCalendar(index, curr_year.value)
    }
    month_list.appendChild(month)
})

let month_picker = calendar.querySelector('#month-picker')

month_picker.onclick = () => {
    month_list.classList.add('show')
}

let currDate = new Date()

let curr_month = {value: currDate.getMonth()}
let curr_year = {value: currDate.getFullYear()}

generateCalendar(curr_month.value, curr_year.value)

document.querySelector('#prev-year').onclick = () => {
    --curr_year.value
    generateCalendar(curr_month.value, curr_year.value)
}

document.querySelector('#next-year').onclick = () => {
    ++curr_year.value
    generateCalendar(curr_month.value, curr_year.value)
}
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</html>