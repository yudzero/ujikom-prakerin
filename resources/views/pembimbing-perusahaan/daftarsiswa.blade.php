@extends('layouts.pembimbingperusahaan.main')
@section('content')


    <section>
        <div class="content-body">
            <div class="container mb-5">
                <div class="card">
                    <p class="mt-4 ml-5" style="color:black; font-weight:700;">Daftar Murid</p>
                    <div class="dropdown">
                        <div class="row">
                            <div class="col-8">
                            </div>
                      <div class="dropdown">
                        <button class="btn btn-light dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                            Tahun
                        </button>
                        <div class="dropdown-menu">
                          <button class="dropdown-item" type="button">2022</button>
                          <button class="dropdown-item" type="button">2023</button>
                          <button class="dropdown-item" type="button">2024</button>
                        </div>
                    </div>
                    <div class="dropdown">
                        <button class="btn btn-light dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                          Divisi
                        </button>
                        <div class="dropdown-menu">
                          <button class="dropdown-item" type="button">IT</button>
                          <button class="dropdown-item" type="button">Marketing</button>
                        </div>
                    </div>
                      </div>
                      <div class="card-body">
                      <table id="myTable" class="table">
                        <thead>
                          <tr style="background-color: #DADADC; border-radius:30px;">
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Kelas</th>
                                <th>Nama Perusahaan</th>
                                <th>Periode</th>
                                <th>Selengkapnya</th>
                            </tr>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($siswa as $s)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $s->NamaSiswa }}</td>
                                <td>{{ $s->kelas }}</td>
                                <td>{{ $s->perusahaan->NamaPerusahaan ?? 'kosng' }}</td>
                                <td>{{ $s->kelas }}</td>
                                <td><span data-toggle="modal" data-target="#exampleModal" style="cursor: pointer;">:</span></td>
                            </tr>
                            @endforeach
                        </tbody>
                      </table>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-body">
        <a href="/perusahaan/berinilai"><H5 style="text-align: center">Beri Nilai</H5></a>
        <hr style="border: 1px solid black;">
        <a href=""><H5 style="text-align: center">Cek Absensi</H5></a>
        <hr style="border: 1px solid black;">
        <a href=""><H5 style="text-align: center">Data Selengkapnya</H5></a>
        <hr style="border: 1px solid black;">
      </div>

    </div>
  </div>
</div>
    </section>

@endsection