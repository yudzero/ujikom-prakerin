
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Surat Permohonan</title>
</head>
<style>
    .header{
        text-align: center;
    }
    .container{
        padding: 30px 80px;
    }
    .ttd{
        text-align: right;
    }
    p{
        font-size: 20px;
    }
</style>
<body>
    <div class="container">
    <div class="header">
        <p>Sekolah Menengah Kejuruan</p>
        <p>Jalan </p><!-- nama jalan --> <p>Kabupaten</p><!-- kabupaten --> <p>Provinsi</p> <!-- provinsi --><p>Kode Pos</p><!-- kode pos -->
        <p>Telepon </p> <!-- telepon --> <p>Faksimile</p> <!-- faksimile -->
    </div>
    <hr>
    <div class="content">
        <p>No:</p>
        <p>Hal: Permohonan Praktik Kerja Lapangan (PKL)</p>
        <p>Lamp : 1 Bundel</p>
        <p>Yth:</p>
        <p>Pimpinan HRD</p><!-- (nama pejabat) -->
        <p><!-- nama dunia kerja --></p>
        <p><!-- alamat lengkap --></p>
        <br>
        <p>Dengan Hormat,</p>
        <p>Dalam rangka pelaksanaan Pendidikan Vokasi terkait dengan program link and match guna meningkatkan kompetensi peserta didik, diwajibkan untuk
            melaksanakan Praktik Kerja Lapangan (PKL). Oleh karena itu kami mengajukan permohonan kepada Bapak/Ibu pimpinan 
            perusahaan agar dapat menerima peserta didik kami sebagai berikut (terlampir);
        </p><br>
        <p>Untuk melaksanakan PKL pada bagian/departemen yang ada di Perusahaan Bapak/Ibu. Adapun pelaksanaan PKL 
            kami rencanakan pada bulan <!-- bulan --> tahun <!-- tahun --> sampai dengan <!-- bulan --> tahun <!-- tahun -->
            atau sesuai dengan waktu yang Bapak/Ibu tentukan. Demikian surat permohonan ini kami ajukan, atas perhatiannya kami ucapkan 
            terima kasih.
        </p><br>
        <p class="ttd">
            Jakarta<!-- kota -->, 21 Januari 2023<!-- tanggal, bulan, tahun --> 
        </p>
        <br><br><br>
        <p class="ttd">Kepala SMK/MAK</p><!-- sekolah -->
    </div>
    </div>
</body>
</html>