@extends('layouts.hubin.main')
@section('content')
<style>
  .container{
    color: black;
  }
  
</style>
<div class="container">
    <div class="row">
      <div class="col-3"></div>
        <div class="col-6">
            <h4 style="text-align: center; font-weight:800;" class="mt-2 mb-4">Masukkan Data Perusahaan</h4>
            <form>
              <div class="mb-3 mt-5">
                <label for="exampleInputEmail1" class="form-label">No Perusahaan</label>
                <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
              </div>
              <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Nama Perusahaan</label>
                <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
              </div>
              <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Alamat Perusahaan</label>
                <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
              </div>
              <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Fax Perusahaan</label>
                <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
              </div>
              <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Email</label>
                <input type="email" class="form-control" id="exampleInputPassword1">
              </div>
              <button type="submit" class="btn btn-primary mt-3 mb-5" style="float: right; background-color:#395B64; border:transparent; width:90px;">Save</button>
            </form>
        </div>
        <div class="col-3"></div>
    </div>
</div>

@endsection