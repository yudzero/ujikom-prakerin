@extends('layouts.hubin.main')
@section('content')


<section>
    <button class="btn btn-primary ml-5" data-toggle="modal" data-target="#tambah-perusahaan"> 
        Tambah Data    
    </button><br><br>
    <div class="content-body">
        <div class="container mb-5">
            <div class="card">
                <p class="mt-4 ml-5" style="color:black; font-weight:700;">Perusahaan</p>
                <table id="myTable" class="tab mb-5 mt-3">
                    <tr>
                        <th>No.</th>
                        <th>Nama Perusahaan</th>
                        <th>Alamat Perusahaan</th>
                        <th>Nomor FAX</th>
                        <th>Aksi</th>
                    </tr>
                    @foreach ($perusahaan as $p)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $p->NamaPerusahaan }}</td>
                        <td>{{ $p->alamat }}</td>
                        <td>{{ $p->fax }}</td>
                        <td> 
                            <button class="btn btn-info" data-toggle="modal" data-target="#perusahaan-edit-{{ $p->NoPerusahaan }}"> Edit Data </button>
                            <a href="/hapus/perusahaan/{{ $p->NoPerusahaan }}" class="btn btn-danger">Hapus</a>
                        </td>
                    </tr>
                    <!-- Modal -->
                    @endforeach
                </table>
                @foreach($perusahaan as $p)
                <div class="modal fade" id="perusahaan-edit-{{ $p->NoPerusahaan }}" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        
                        <form action="/hubin/{{ $p->NoPerusahaan }}/edit_perusahaan" method="POST">
                            @csrf
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Edit Data {{ $p->NamaPerusahaan }}</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <label for="inputPassword5" class="form-label">Nama Perusahaan</label>
                                <input type="text" value="{{ $p->NamaPerusahaan }}" name="NamaPerusahaan" id="inputPassword5" class="form-control"
                                    aria-describedby="passwordHelpBlock">
                                <label for="inputPassword5" class="form-label">Alamat</label>
                                <input type="text" value="{{ $p->alamat }}" name="alamat" id="inputPassword5" class="form-control"
                                    aria-describedby="passwordHelpBlock">
                                <label for="inputPassword5" class="form-label">Faximile</label>
                                <input type="text" value="{{ $p->fax }}" name="fax" id="inputPassword5" class="form-control"
                                    aria-describedby="passwordHelpBlock">
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                                <button class="btn btn-primary" type="submit">Simpan</button>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
                @endforeach
                <hr>
                <p class="teks1">Rows per page: <span class=""> 8 <i class="fa-solid fa-caret-down"></i><span
                            class="teks1 ml-4">1-8 of 1240</span><span><i class="fa-solid fa-chevron-left mr-3"></i><i
                                class="fa-solid fa-chevron-right"></i> </span></p>
                <!-- Button trigger modal -->



            </div>
        </div>
    </div>

    @foreach($perusahaan as $p)
    <div class="modal fade" id="tambah-perusahaan" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="/tambah/perusahaan" method="POST">
                @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label for="inputPassword5" class="form-label">Nomor Perusahaan</label>
                    <input type="text"  name="NoPerusahaan" id="inputPassword5" class="form-control"
                        aria-describedby="passwordHelpBlock">
                    <label for="inputPassword5" class="form-label">Nama Perusahaan</label>
                    <input type="text"  name="NamaPerusahaan" id="inputPassword5" class="form-control"
                        aria-describedby="passwordHelpBlock">
                    <label for="inputPassword5" class="form-label">Alamat</label>
                    <input type="text" name="alamat" id="inputPassword5" class="form-control"
                        aria-describedby="passwordHelpBlock">
                    <label for="inputPassword5" class="form-label">Email</label>
                    <input type="text" name="email" id="inputPassword5" class="form-control"
                        aria-describedby="passwordHelpBlock">
                    <label for="inputPassword5" class="form-label">Jumlah Siswa</label>
                    <input type="text" name="jumlahmurid" id="inputPassword5" class="form-control"
                        aria-describedby="passwordHelpBlock">
                    <label for="inputPassword5" class="form-label">Faximile</label>
                    <input type="text" name="fax" id="inputPassword5" class="form-control"
                        aria-describedby="passwordHelpBlock">
                    <label for="inputPassword5" class="form-label">Maps</label>
                    <input type="text" name="maps" id="inputPassword5" class="form-control"
                        aria-describedby="passwordHelpBlock">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary" type="submit">Simpan</button>
                </div>
            </div>
        </form>
        </div>
    </div>
    @endforeach
</section>

@endsection