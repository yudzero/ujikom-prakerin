@extends('layouts.hubin.main')
@section('content')


    <section>
        <div class="content-body">
            <div class="container mb-5">
                <div class="row">
                    <div class="col mb-3">
                        <button class="btn btn-primary ml-5" data-toggle="modal" data-target="#tambah-perusahaan"> 
                            Tambah Data    
                        </button>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                    </div>
                </div>
                <div class="card p-3">
                    <p class="mt-2 ml-5 mb-3" style="color:black; font-weight:700;">Daftar Murid</p> 
                    <table id="myTable" class="table">
                        <thead>
                          <tr style="background-color: #DADADC; border-radius:30px;">
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Kelas</th>
                                <th>Tanggal Lahir</th>
                                <th>Tempat Lahir</th>
                                <th>Alamat</th>
                                <th>Email</th>
                                <th>Jurusan</th>
                                <th>Aksi</th>
                            </tr>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($siswa as $s)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $s->NamaSiswa }}</td>
                                <td>{{ $s->kelas }}</td>
                                <td>{{ $s->Tgllahir }}</td>
                                <td>{{ $s->Tmplahir }}</td>
                                <td>{{ $s->Alamat_Siswa }}</td>
                                <td>{{ $s->email }}</td>
                                <td>{{ $s->jurusan->jurusan ?? 'Kosong' }}  </td>
                                <td>
                                    <button class="btn btn-warning" data-toggle="modal"data-target="#logoutModal-edit-{{ $s->nis }}"><i class="fa-solid fa-pencil"></i></button>
                                    <a href="/hapus/siswa/{{ $s->nis }}" class="btn btn-danger"><i class="fa-solid fa-trash"></i></a></td>
                            </tr>
                            @endforeach
                        </tbody>
                      </table>
                    
                    <!--Modal-->
                    @foreach($siswa as $s)
                    <div class="modal fade" id="logoutModal-edit-{{ $s->nis }}" tabindex="-1" role="dialog"
                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            
                            <form action="/hubin/{{ $s->nis }}/edit_siswa" method="POST">
                                @csrf
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Edit Data {{ $s->NamaSiswa }}</h5>
                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <label for="inputPassword5" class="form-label">Nama Siswa</label>
                                    <input type="text" value="{{ $s->NamaSiswa }}" name="NamaSiswa" id="inputPassword5" class="form-control"
                                        aria-describedby="passwordHelpBlock">
                                    <label for="inputPassword5" class="form-label">Kelas</label>
                                    <input type="text" value="{{ $s->kelas }}" name="kelas" id="inputPassword5" class="form-control"
                                        aria-describedby="passwordHelpBlock">
                                    <label for="inputPassword5" class="form-label">Tanggal Lahir</label>
                                    <input type="date" value="{{ $s->Tgllahir }}" name="Tgllahir" id="inputPassword5" class="form-control"
                                        aria-describedby="passwordHelpBlock">
                                    <label for="inputPassword5" class="form-label">Tempat Lahir</label>
                                    <input type="text" value="{{ $s->Tmplahir }}" name="Tmplahir" id="inputPassword5" class="form-control"
                                        aria-describedby="passwordHelpBlock">
                                    <label for="inputPassword5" class="form-label">Alamat</label>
                                    <input type="text" value="{{ $s->Alamat_Siswa }}" name="Alamat_Siswa" id="inputPassword5" class="form-control"
                                        aria-describedby="passwordHelpBlock">
                                    <label for="inputPassword5" class="form-label">Nama Perusahaan</label>
                                    <select class="form-control form-control-sm" name="">
                                        @foreach($perusahaan as $p)
                                        <option>{{ $p->NamaPerusahaan }}</option>
                                        @endforeach
                                      </select>
                                    {{-- <label for="inputPassword5" class="form-label">Jurusan</label>
                                    <input type="text" value="{{ $s->jurusan->jurusan }}" name="Alamat_Siswa" id="inputPassword5" class="form-control"
                                        aria-describedby="passwordHelpBlock"> --}}
                                    
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                                    <button class="btn btn-primary" type="submit">Simpan</button>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                    @endforeach
                    @foreach($siswa as $s)
                    <div class="modal fade" id="tambah-perusahaan" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <form action="/tambah/siswa" method="POST">
                            @csrf
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <label for="inputPassword5" class="form-label">Nis</label>
                                <input type="number" placeholder="Masukkan NIS" name="nis" class="form-control" id="validationCustomUsername" aria-describedby="inputGroupPrepend" required>
                                <label for="inputPassword5" class="form-label">Nama</label>
                                <input type="text"  name="NamaSiswa" id="inputPassword5" class="form-control"
                                    aria-describedby="passwordHelpBlock" required>
                                <label for="inputPassword5" class="form-label">Kelas</label>
                                <input type="text"  name="kelas" id="inputPassword5" class="form-control"
                                    aria-describedby="passwordHelpBlock" required>
                                <label for="inputPassword5" class="form-label">Tanggal Lahir</label>
                                <input type="date" name="Tgllahir" id="inputPassword5" class="form-control"
                                    aria-describedby="passwordHelpBlock" required>
                                <label for="inputPassword5" class="form-label">Tempat Lahir</label>
                                <input type="text" name="Tmplahir" id="inputPassword5" class="form-control"
                                    aria-describedby="passwordHelpBlock" required>
                                <label for="inputPassword5" class="form-label">Alamat</label>
                                <input type="text" name="Alamat_Siswa" id="inputPassword5" class="form-control"
                                    aria-describedby="passwordHelpBlock" required>
                                <label for="inputPassword5" class="form-label">Email</label>
                                <input type="email" name="email" id="inputPassword5" class="form-control"
                                    aria-describedby="passwordHelpBlock" required>
                                    {{-- <label for="inputPassword5" class="form-label">Perusahaan</label>
                                <input type="text"  name="Alamat_Siswa" id="inputPassword5" class="form-control"
                                        aria-describedby="passwordHelpBlock">
                                    <label for="inputPassword5" class="form-label">Jurusan</label>
                                <input type="text"  name="Alamat_Siswa" id="inputPassword5" class="form-control"
                                        aria-describedby="passwordHelpBlock"> --}}
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                                <button class="btn btn-primary" type="submit">Simpan</button>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
                    @endforeach
                </div>
            </div>
        </div>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.6.1.slim.js" integrity="sha256-tXm+sa1uzsbFnbXt8GJqsgi2Tw+m4BLGDof6eUPjbtk=" crossorigin="anonymous"></script>
    </section>
    

@endsection