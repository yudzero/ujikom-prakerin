@extends('layouts.hubin.main')
@section('content')
    

    <style>
        .cube {
            margin: 0 auto;
            margin-top: 70px; 
            position: relative;
            width: 75%;
            height: 50%;
            border: dashed 3px #C4C4C4;
            }

        .cube:after {
            content: "";
            display: block;
            padding-bottom: 50%;
            }

        .content {
            position: absolute;
            width: 100%;
            height: 100%;
            }

        .header-upload {
            margin-top: 200px;
            font-size: 30px;
        }

        .upload-area {
            margin-left: 50px;
            margin-top: 30px;
        }
        
        .fa-arrow-left {
            font-size: 30px;
        }

        .upload-button {
            margin-top: 100px;
        }
    </style>

    <title>{{ $title }}</title>
  </head>
  <body>
    <div class="container">
        
    <div class="cube">
        <div class="content">
            <div class="row text-center header-upload">
                <div class="col">
                    <img src="{{ asset('assets/img/excel-logo.png') }}" width="100px" height="100px" alt=""> <span>Masukkan File Excel!</span>
                </div>
            </div>
            <div class="row text-center upload-area">
                <form action="/importsiswa" method="POST" enctype="multipart/form-data">
                    @csrf
                <div style="margin-left: 161px;" class="col">
                    <input style="text-align:center;" type="file" name="file">
                </div>
            </div>
            <div class="row text-right upload-button mr-2">
                <div class="col">
                    <button type="submit" class="btn btn-secondary mt-4 mb-5">Upload!</button>
                </div>
            </div>
        </form>
        </div>
    </div>
    </div>
    @endsection
    
  